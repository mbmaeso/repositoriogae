package com.Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import consultas.UsuarioUtils;
import entidadesBD.Usuario;

@ManagedBean
@RequestScoped
public class EditarUsuarioBean{
	
	@ManagedProperty("#{agendaBean.usuario}")	
	private Usuario usuario;
	private String mensajeError;
	
	public EditarUsuarioBean() {
	}

	public String getKey() {
		return usuario.getKey().toString();
	}
	public String editarUsuario() {
		UsuarioUtils.edit(usuario);
		return "verMiPerfil";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
}
