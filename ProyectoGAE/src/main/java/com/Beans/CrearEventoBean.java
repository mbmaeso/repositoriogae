
package com.Beans;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import consultas.EventoUtils;
import entidadesBD.Evento;
import entidadesBD.Usuario;

@ManagedBean
@RequestScoped
public class CrearEventoBean {

	@ManagedProperty("#{agendaBean}")
	private AgendaBean agendaBean;
	@ManagedProperty("#{agendaBean.usuario}")
	private Usuario usuario;
	private String titulo;
	private String descripcion;
	private String preferencias;
	private float precio;
	private Date fechaInicio;
	private Date fechaFin;
	private String direccion;
	private Usuario validador;

	public CrearEventoBean() {
	}

	public String doCrear() {
		if (usuario.getTipousuario().equals("US")) {
			EventoUtils.put(titulo, descripcion, preferencias, precio, fechaInicio, fechaFin, false, null,
					usuario.getKey(), direccion);
		} else {
			EventoUtils.put(titulo, descripcion, preferencias, precio, fechaInicio, fechaFin, true,
					usuario.getKey(), usuario.getKey(), direccion);
		}
		agendaBean.filtrar();
		return "agendaHome";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public AgendaBean getAgendaBean() {
		return agendaBean;
	}

	public void setAgendaBean(AgendaBean agendaBean) {
		this.agendaBean = agendaBean;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Usuario getValidador() {
		return validador;
	}

	public void setValidador(Usuario validador) {
		this.validador = validador;
	}

	public String getPreferencias() {
		return preferencias;
	}

	public void setPreferencias(String preferencias) {
		this.preferencias = preferencias;
	}
}
