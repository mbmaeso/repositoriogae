package com.Beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import consultas.EventoUtils;
import consultas.UsuarioUtils;
import entidadesBD.Evento;
import entidadesBD.Notificacion;
import entidadesBD.Usuario;

@ManagedBean
@SessionScoped
public class AgendaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Evento> listaEventos;
	private List<Evento> listaEventosPerfil;
	private Evento evento;
	private Usuario usuario;
	private String email;
	private String filtro;
	private double precioMax;
	private String correoUsuarioBuscado;
	private Usuario usuarioBuscado;
	private boolean mensajeErrorBusqueda;
	private List<Notificacion> listaNotificaciones;
	private String correoUsuario;
	private String mensajeError = "";

	private List<Evento> listaGustados;
	private List<Evento> listaSeguidos;
	private List<Evento> listaCreados;
	private List<Evento> listaValidados;// lista eventos validados por el usuario en sesion
	private List<String> listaDireccionesEventos = new ArrayList<>();

	@PostConstruct
	public void init() {
		listaEventos = filtrado(" ");
		cargarDirecciones();
		filtro = "";
		precioMax = 0.0;
		setMensajeError(mensajeError);
		mensajeErrorBusqueda = false;
		// mensajeError = "Esto es una prueba 1";
		// email="javycg95@gmail.com";
		// iniciarSesion();


	}

	public AgendaBean() {
	}

	public boolean estaConectado() {
		UserService userService = UserServiceFactory.getUserService();
		iniciarSesion();
		return userService.isUserLoggedIn();
	}
	public boolean estaConectadoFiltrado() {
		UserService userService = UserServiceFactory.getUserService();
		iniciarSesion();
		
		return userService.isUserLoggedIn() && filtro.equals("precio");
	}
	public boolean esPeriodista() {
		UserService userService = UserServiceFactory.getUserService();
		if (userService.isUserLoggedIn()) {
			if (usuario != null) {
				return usuario.getTipousuario().equals("PE");
			}
		}
		return false;
	}

	/*
	 * public boolean hayNotificaciones() { listaNotificaciones(); if
	 * (listaNotificaciones == null) { return false; } return
	 * !listaNotificaciones.isEmpty(); }
	 */

	public String filtrar() {
		listaEventos = filtrado(filtro);
		cargarDirecciones();
		return "agendaHome";
	}
	
	public void cargarDirecciones() {
		if (!listaDireccionesEventos.isEmpty()) {
			listaDireccionesEventos.clear();
		}				
		if (!listaEventos.isEmpty()) {
			for (Evento evento : listaEventos) {
				listaDireccionesEventos.add(evento.getDireccion());
			}
		}
	}

	public String volverAgendaInicio() {
		precioMax = 0.0;
		listaEventos = EventoUtils.getEventosValidados();
		return "agendaHome";
	}

	public String filtradoPerfil() {
		listaEventosPerfil = filtrado(filtro);
		return "verMiPerfil";
	}

	public String iniciarSesion() {
		UserService userService = UserServiceFactory.getUserService();
		if (userService.isUserLoggedIn()) {
			User user = userService.getCurrentUser();
			email = user.getEmail();
			usuario = UsuarioUtils.buscarUsuario(email);
			if(usuario == null) {
				UsuarioUtils.put(email, "Nombre", "Apellidos", "US", "", "");
				usuario = UsuarioUtils.buscarUsuario(email);
			}
		}
		return "agendaHome";
	}

	public String cerrarSesion() {
		email = "";
		usuario = null;
		return "agendaHome";
	}

	public String getLoginUrl() {
		UserService userService = UserServiceFactory.getUserService();
		return userService.createLoginURL("/faces/agendaHome.xhtml");
	}

	public String getLogoutUrl() {
		UserService userService = UserServiceFactory.getUserService();
		return userService.createLogoutURL("/faces/agendaHome.xhtml");
	}

	private List<Evento> filtrado(String filtro) {
		switch (filtro) {

		case "tag":
			return buscarEventoPorTag();
		case "precio":
			return buscarPorPrecioMax();
		case "validados":
			return buscarValidadosPorAlguien();
		case "creados":
			return buscarCreadosPorAlguien();
			/*
		case "seguidos":
			return listaSeguidos();
		case "gustados":
			return listaGustados();
		case "cercano":
			return encontrarEventosCercanos(coordenadas);
*/
		default:
			return EventoUtils.getEventosValidados();
		}
	}

	private List<Evento> buscarEventoPorTag() {
		return EventoUtils.getEventosPorTag(usuario.getTag());
	}
	private List<Evento> buscarCreadosPorAlguien() {
		return EventoUtils.getEventosCreadosPorAlguien(usuario.getKey());
	}
	private List<Evento> buscarValidadosPorAlguien() {
		return EventoUtils.getEventosValidadosPorAlguien(usuario.getKey());
	}
	private List<Evento> buscarPorPrecioMax() {

		return EventoUtils.getEventosPorPrecioMax(precioMax);
	}

	public List<Notificacion> obtenerNotificaciones(Usuario usuario) {
		return new ArrayList<Notificacion>();
	}

	public String crearCuenta() {
		return "crearUsuario";
	}

	public String editarCuenta() {
		return "editarCuenta";
	}

	public String irVerEvento() {
		String title = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("title");
		Evento e = EventoUtils.buscarEvento(title);
		setEvento(e);
		return "verEvento";
	}

	public String irCrearEvento() {

		return "crearEvento";
	}

	public String irMiPerfil() {
		listaEventosPerfil = new ArrayList<Evento>();
		return "verMiPerfil";
	}

	public String irBusqueda() {
		usuarioBuscado = UsuarioUtils.buscarUsuario(correoUsuarioBuscado);
		if (usuarioBuscado == null) {
			mensajeErrorBusqueda = true;
			return "agendaHome";
		}
		return "verUsuario";
	}

	public String irEventosNoValidados() {
		return "verEventosNoValidados";
	}

	public String irMisNotificaciones() {
		return "verNotificaciones";
	}

	public List<Evento> getListaEventos() {
		return listaEventos;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public List<Evento> getListaEventosPerfil() {
		return listaEventosPerfil;
	}

	public void setListaEventosPerfil(List<Evento> listaEventosPerfil) {
		this.listaEventosPerfil = listaEventosPerfil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public List<Evento> getListaGustados() {
		return listaGustados;
	}

	public void setListaGustados(List<Evento> listaGustados) {
		this.listaGustados = listaGustados;
	}

	public List<Evento> getListaSeguidos() {
		return listaSeguidos;
	}

	public void setListaSeguidos(List<Evento> listaSeguidos) {
		this.listaSeguidos = listaSeguidos;
	}

	public List<Evento> getListaCreados() {
		return listaCreados;
	}

	public void setListaCreados(List<Evento> listaCreados) {
		this.listaCreados = listaCreados;
	}

	public List<Notificacion> getListaNotificaciones() {
		return listaNotificaciones;
	}

	public void setListaNotificaciones(List<Notificacion> listaNotificaciones) {
		this.listaNotificaciones = listaNotificaciones;
	}

	public List<Evento> getListaValidados() {
		return listaValidados;
	}

	public void setListaValidados(List<Evento> listaValidados) {
		this.listaValidados = listaValidados;
	}

	public List<String> getListaDireccionesEventos() {
		return listaDireccionesEventos;
	}

	public void setListaDireccionesEventos(List<String> listaDireccionesEventos) {
		this.listaDireccionesEventos = listaDireccionesEventos;
	}

	public void setListaEventos(List<Evento> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getCorreoUsuario() {
		return correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	public double getPrecioMax() {
		return precioMax;
	}

	public void setPrecioMax(double precioMax) {
		this.precioMax = precioMax;
	}

	public String getCorreoUsuarioBuscado() {
		return correoUsuarioBuscado;
	}

	public void setCorreoUsuarioBuscado(String correoUsuarioBuscado) {
		this.correoUsuarioBuscado = correoUsuarioBuscado;
	}

	public Usuario getUsuarioBuscado() {
		return usuarioBuscado;
	}

	public void setUsuarioBuscado(Usuario usuarioBuscado) {
		this.usuarioBuscado = usuarioBuscado;
	}

	public boolean getMensajeErrorBusqueda() {
		return mensajeErrorBusqueda;
	}
}
