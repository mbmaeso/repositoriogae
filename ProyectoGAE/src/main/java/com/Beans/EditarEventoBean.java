package com.Beans;

import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import consultas.EventoUtils;
import entidadesBD.Evento;
import entidadesBD.Usuario;

@ManagedBean
@RequestScoped
public class EditarEventoBean {

	@ManagedProperty("#{agendaBean}")
	private AgendaBean agendaBean;
	@ManagedProperty("#{verEventoBean.usuario}")
	private Usuario usuario;
	@ManagedProperty("#{verEventoBean.evento}")
	private Evento evento;
	private String titulo;
	private String descripcion;
	private String[] tag;
	private float precio;
	private Date fechaInicio;
	private Date fechaFin;
	private String direccion;
	private boolean validado;
	private Usuario validador;
	private String tags;

	public EditarEventoBean() {
	}

	@PostConstruct
	public void init() {
		titulo = evento.getTitulo();
		descripcion = evento.getDescripcion();
		tags = evento.getTag();
		tagsToArray();
		precio = (float) evento.getPrecio();
		fechaInicio = evento.getFechaInicio();
		fechaFin = evento.getFechaFin();
		direccion = (String) evento.getDireccion();
		//this.setTag(evento.getTag().split(";"));
	}

	public void tagsToArray() {
		if (evento.getTag() != null) {
			tag = evento.getTag().split(";");
		} else {
			tag = new String[1];
			tag[0] = "";
		}
	}

	public String doEditar() {
		EventoUtils.edit(evento);
		agendaBean.filtrar();
		return "agendaHome";
	}

	public void deleteOneTag(String tag) {
		/*tagsUpdated = "";
		tagsUpdated = evento.getTag().replace(tag + ";", "");
		EventoUtils.put(titulo, tagsUpdated, descripcion, precio, fechaInicio, fechaFin, validado, validador.getKey(),
				usuario.getKey(), latitud, longitud, direccion);
		evento = EventoUtils.search(evento.key);
		tagsToArray();*/
		
		//Hay que modificarlo, lo de arriba no sirve
	}

	public void addOneNewTag() {
		/*tagsUpdated = "";
		if (evento.getTag().length() == 0) {
			tagsUpdated = nuevaPreferencia + ";";
		} else {
			tagsUpdated = evento.getTag() + nuevaPreferencia + ";";
		}
		EventoUtils.put(titulo, tagsUpdated, descripcion, precio, fechaInicio, fechaFin, validado, validador.getKey(),
				usuario.getKey(), latitud, longitud, direccion);
		evento = EventoUtils.search(evento.key);
		tagsToArray();*/
		
		//Hay que modificarlo, lo de arriba no sirve
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
		evento.setTitulo(titulo);
	}

	public String getDescripcion() {
		return descripcion;
		
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
		evento.setDescripcion(descripcion);
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
		evento.setTag(tags);
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
		evento.setPrecio(precio);
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
		evento.setFechaInicio(fechaInicio);
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
		evento.setFechaFin(fechaFin);
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
		evento.setDireccion(direccion);
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public Usuario getValidador() {
		return validador;
	}

	public void setValidador(Usuario validador) {
		this.validador = validador;
	}
	public String volverAgendaInicio() {		
		agendaBean.setListaEventos(EventoUtils.getEventosValidados());
		agendaBean.cargarDirecciones();
		agendaBean.setPrecioMax(0.0);
		return "agendaHome";
	}

	public AgendaBean getAgendaBean() {
		return agendaBean;
	}

	public void setAgendaBean(AgendaBean agendaBean) {
		this.agendaBean = agendaBean;
	}
	
}
