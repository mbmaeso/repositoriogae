package com.Beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;

import consultas.ComentarioUtils;
import consultas.EventoUtils;
import consultas.SeguirEventoUtils;
import consultas.UsuarioUtils;
import entidadesBD.Comentario;
import entidadesBD.Evento;
import entidadesBD.SeguirEvento;
import entidadesBD.Usuario;


@ManagedBean
@SessionScoped
public class VerEventoBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{agendaBean.evento}")
	protected Evento evento;
	@ManagedProperty("#{agendaBean.usuario}")
	private Usuario usuario;
	private List<SeguirEvento> listaSeguidosI = new ArrayList<>();
	private List<Usuario> listaSeguidores = new ArrayList<Usuario>();
	private SeguirEvento eventoSeguido;
	private List<Comentario> listaComentarios = new ArrayList<>();
	private String comentario;
	
	private boolean sigo;

	@PostConstruct
	public void init() throws EntityNotFoundException {	
		recargarVerEvento();
	}

	public VerEventoBean() {
	}

	public boolean editable() {
		if (usuario == null)
			return false;
		return usuario.getTipousuario().equals("PE");
	}
	
	//No se usa
	public String irVerEvento() {
		String title = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("title");
		Evento e = EventoUtils.buscarEvento(title);
		setEvento(e);
		return "verEvento";
	}
	public void recargarVerEvento() throws EntityNotFoundException {
		obtenerListaSeguidos();
		sigo = this.isSigo();
		obtenerListaComentarios();
	}
	
	public String anadirComentario() throws EntityNotFoundException {
		ComentarioUtils.put(evento.getKey(), usuario.getKey(), comentario, new Date());
		recargarVerEvento();
		return "VerEvento";
	}
	
	public void obtenerListaComentarios() {
		if (!listaComentarios.isEmpty()) {
			listaComentarios.clear();
		}
		listaComentarios = ComentarioUtils.getComentarios(evento.getKey());
	}

	public void obtenerListaSeguidos() throws EntityNotFoundException {
		if (!listaSeguidores.isEmpty()) {
			listaSeguidores.clear();
		}
		listaSeguidosI = SeguirEventoUtils.getListaSeguidos(evento.getKey());
		for(SeguirEvento s: listaSeguidosI) {
			Usuario us = UsuarioUtils.searchKey(s.getUsuario());
			if(us != null) {
				listaSeguidores.add(us);
			}
		}
	}
	
	public String nombreUsuarioComentario(Key usuario) {
		return UsuarioUtils.searchKey(usuario).getNombre();
	}
	
	public String doSeguir() throws EntityNotFoundException {
		SeguirEventoUtils.put(evento.getKey(), usuario.getKey());
		recargarVerEvento();
		return "VerEvento";
	}

	public String dejarSeguir() throws EntityNotFoundException{
		SeguirEventoUtils.delete(SeguirEventoUtils.getSeguido(evento.getKey(),usuario.getKey()).getKey());
		recargarVerEvento();
		return "VerEvento";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public Evento getEvento() {
		return evento;
	}	
	
	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public List<Comentario> getListaComentarios() {
		return listaComentarios;
	}

	public void setListaComentarios(List<Comentario> listaComentarios) {
		this.listaComentarios = listaComentarios;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getListaSeguidores() {
		return listaSeguidores;
	}

	public void setListaSeguidores(List<Usuario> listaSeguidos) {
		this.listaSeguidores = listaSeguidos;
	}

	public boolean isSigo() throws EntityNotFoundException {
		boolean res = false;
		for(Usuario s: listaSeguidores) 
			res = s.getKey().equals(usuario.getKey());
			
		return res;
	}

	public void setSigo(boolean sigo) {
		this.sigo = sigo;
	}

	public String volverAgendaInicio() {
		return "agendaHome";
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
}
