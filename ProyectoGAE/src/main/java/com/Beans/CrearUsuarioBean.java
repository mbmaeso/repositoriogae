package com.Beans;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import consultas.UsuarioUtils;
import entidadesBD.Usuario;

@ManagedBean
@RequestScoped
public class CrearUsuarioBean {

	private String nombre;
	private String apellidos;
	private String email;
	private String direccion;
	private String tag;
	private String mensajeError;

	public CrearUsuarioBean() {
	}

	public String crearUsuario() {
		UsuarioUtils.put(email, nombre, apellidos, "US", tag, direccion);
		return "agendaHome";
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

}
