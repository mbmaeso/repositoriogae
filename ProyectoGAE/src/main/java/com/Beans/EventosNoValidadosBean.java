package com.Beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import com.google.appengine.api.datastore.Key;

import consultas.EventoUtils;
import consultas.NotificacionUtils;
import entidadesBD.Evento;
import entidadesBD.Usuario;


@ManagedBean
@RequestScoped
public class EventosNoValidadosBean {

	@ManagedProperty("#{agendaBean}")
	private AgendaBean agendaBean;
	
	@ManagedProperty("#{agendaBean.usuario}")
	private Usuario usuario;
	
	private List<Evento> listaEventos;

	@PostConstruct
	public void init() {
		listaEventos = listaEventosNoValidados();
	}

	public EventosNoValidadosBean() {
	}

	public String aprobarEvento(Evento evento) {
		crearNotificacion("Se ha aprobado el evento '" + evento.getTitulo() + "'", new Date(), evento.getKey(),
				usuario.getKey());
		
		evento.setValidado(true);
		evento.setValidador(usuario.getKey());
		EventoUtils.edit(evento);
		listaEventos = listaEventosNoValidados();

		return "eventosPendientesDeValidacion";
	}

	public String rechazarEvento(Evento evento) {
		crearNotificacion("Se ha rechazado el evento '" + evento.getTitulo() + "'", new Date(), evento.getKey(),
				usuario.getKey());
		listaEventos = listaEventosNoValidados();
		EventoUtils.delete(evento.getKey());
		return "eventosPendientesDeValidacion";
	}

	private List<Evento> listaEventosNoValidados() {
		return EventoUtils.getEventosNoValidados();
	}

	private void crearNotificacion(String contenido, Date fecha, Key evento, Key usuario) {
		NotificacionUtils.put(contenido, fecha, evento, usuario);
	}

	public List<Evento> getListaEventos() {
		return listaEventos;
	}

	public void setListaEventos(List<Evento> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public String volverAtras() {
		return "agendaHome";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public AgendaBean getAgendaBean() {
		return agendaBean;
	}

	public void setAgendaBean(AgendaBean agendaBean) {
		this.agendaBean = agendaBean;
	}
	
}