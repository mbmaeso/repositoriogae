package consultas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import entidadesBD.DSF;
import entidadesBD.Evento;
import entidadesBD.Usuario;

public class UsuarioUtils {

	public static void put(final String email, final String nombre, final String apellidos, final String tipousuario,
			final String tag, final String direccion) {

		final DatastoreService datastoreService = DSF.getDatastoreService();
		final Usuario usuario = new Usuario(email, nombre, apellidos, tipousuario, tag, direccion);
		datastoreService.put(usuario.getEntity());
	}

	public static void edit(Usuario usuario) {

		final DatastoreService datastoreService = DSF.getDatastoreService();
		datastoreService.put(usuario.getEntity());
	}

	public static Usuario searchKey(final Key key){

		final DatastoreService datastoreService = DSF.getDatastoreService();
		try {
			return new Usuario(datastoreService.get(key));
		} catch (EntityNotFoundException e) {
			return null;
		}
	}

	public static Usuario buscarUsuario(final String email) {

		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Usuario.EMAIL, FilterOperator.EQUAL, email);

		Query query = new Query(Usuario.USUARIO_ENTITY).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query).asList(FetchOptions.Builder.withLimit(1));

		if (entities.isEmpty()) {
			return null;
		}
		return new Usuario(entities.get(0));
	}
}
