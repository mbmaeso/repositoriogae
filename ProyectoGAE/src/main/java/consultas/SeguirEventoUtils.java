package consultas;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import entidadesBD.DSF;
import entidadesBD.SeguirEvento;

public class SeguirEventoUtils {

	private static final int FETCH_MAX_RESULTS = 50;

	public static void put(final Key evento, final Key usuario) {

		final DatastoreService datastoreService = DSF.getDatastoreService();
		final SeguirEvento seguirEvento = new SeguirEvento(evento, usuario);
		datastoreService.put(seguirEvento.getEntity());
	}

	public static void delete(final Key key) {
		final DatastoreService datastoreService = DSF.getDatastoreService();
		datastoreService.delete(key);
	}
	
	public static SeguirEvento getSeguido(Key evento, Key usuario) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(SeguirEvento.USUARIO, FilterOperator.EQUAL, usuario);
		Filter filter2 = new FilterPredicate(SeguirEvento.EVENTO, FilterOperator.EQUAL, evento);
		
		CompositeFilter compositeFilter = CompositeFilterOperator.and(filter, filter2);

		Query query = new Query(SeguirEvento.RELACION_ENTITY).setFilter(compositeFilter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(1));

		if (entities.isEmpty()) {
			return null;
		}
		return new SeguirEvento(entities.get(0));

	}
	
	public static List<SeguirEvento> getListaSeguidos(Key evento) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(SeguirEvento.EVENTO, FilterOperator.EQUAL, evento);

		Query query = new Query(SeguirEvento.RELACION_ENTITY).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<SeguirEvento> seguidores = new ArrayList<SeguirEvento>();
		if (entities.isEmpty()) {
			return seguidores;
		} else {
			for (Entity e : entities) {
				seguidores.add(new SeguirEvento(e));
			}
		}
		return seguidores;
	}
	public static List<SeguirEvento> getListaSeguidosPorUsuario(Key usuario) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(SeguirEvento.USUARIO, FilterOperator.EQUAL, usuario);

		Query query = new Query(SeguirEvento.RELACION_ENTITY).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<SeguirEvento> seguidores = new ArrayList<SeguirEvento>();
		if (entities.isEmpty()) {
			return seguidores;
		} else {
			for (Entity e : entities) {
				seguidores.add(new SeguirEvento(e));
			}
		}
		return seguidores;
	}
}
