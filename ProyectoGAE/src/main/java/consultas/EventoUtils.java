package consultas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import entidadesBD.DSF;
import entidadesBD.Evento;
import entidadesBD.SeguirEvento;

public class EventoUtils {

	private static final int FETCH_MAX_RESULTS = 50;

	/**
	 * Insertar evento en en datastore
	 * 
	 * @param titulo,
	 *            tag, descripcion, precio, fechaInicio, fechaFin, validado,
	 *            validador, autor, latitud, longitud
	 */
	public static void delete(final Key key) {

		// recuperacion del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// creamos un nuevo evento
		// los insertamos en el datastore
		datastoreService.delete(key);
	}

	/**
	 * Insertar evento en en datastore
	 * 
	 * @param titulo,
	 *            tag, descripcion, precio, fechaInicio, fechaFin, validado,
	 *            validador, autor, latitud, longitud
	 */
	public static void put(final String titulo, final String tag, final String descripcion, final float precio,
			final Date fechaInicio, final Date fechaFin, final boolean validado, final Key validador, final Key autor,
			final String direccion) {

		// recuperacion del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// creamos un nuevo evento
		final Evento evento = new Evento(titulo, tag, descripcion, precio, fechaInicio, fechaFin, validado, validador,
				autor, direccion);
		// los insertamos en el datastore
		datastoreService.put(evento.getEntity());
	}

	public static void edit(Evento evento) {

		// recuperacion del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// los insertamos en el datastore
		datastoreService.put(evento.getEntity());
	}

	public static Evento buscarEvento(final String titulo) {

		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Evento.TITULO, FilterOperator.EQUAL, titulo);

		// Query
		Query query = new Query(Evento.EVENTO_ENTITY).setFilter(filter);

		// ejecuta la consulta, devolviendo una lista con un max. de FETCH_MAX_RESULTS
		// entidades
		List<Entity> entities = datastoreService.prepare(query).asList(FetchOptions.Builder.withLimit(1));

		if (entities.isEmpty()) {
			return null;
		}
		return new Evento(entities.get(0));
	}

	/**
	 * Buscar eventos validados
	 * 
	 * @return List<EventoBD> donde los EventoBD estan validados
	 */
	public static List<Evento> getEventosValidados() {
		// recuperación del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// Filtro para la consulta
		Filter filter = new FilterPredicate(Evento.VALIDADO, FilterOperator.EQUAL, true);

		// Query
		Query query = new Query(Evento.EVENTO_ENTITY).setFilter(filter);

		// Ordenar query
		query.addSort(Evento.FECHAINICIO, Query.SortDirection.ASCENDING);

		// ejecuta la consulta, devolviendo una lista con un max. de FETCH_MAX_RESULTS
		// entidades
		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventos = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventos;
		} else {
			for (Entity e : entities) {
				eventos.add(new Evento(e));
			}
		}
		return eventos;
	}

	/**
	 * Buscar eventos por precio
	 * 
	 * @return List<EventoBD> donde los EventoBD tienen un precio mayor a @precio y
	 *         estan validados
	 */
	public static List<Evento> getEventosPorPrecioMax(double precio) {
		// recuperación del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// Filtro para la consulta por precio y validados
		Filter filter = new FilterPredicate(Evento.PRECIO, FilterOperator.GREATER_THAN_OR_EQUAL, precio);
		Filter filter2 = new FilterPredicate(Evento.VALIDADO, FilterOperator.EQUAL, true);
		CompositeFilter compositeFilter = CompositeFilterOperator.and(filter, filter2);

		// Query
		Query query = new Query(Evento.EVENTO_ENTITY).setFilter(compositeFilter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventos = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventos;
		} else {
			for (Entity e : entities) {
				eventos.add(new Evento(e));
			}
		}
		return eventos;
	}

	/**
	 * Buscar eventos por precio
	 * 
	 * @return List<EventoBD> donde los EventoBD tienen un precio mayor a @precio y estan validados
	 */
	public static List<Evento> getEventosPorTag(String tagUsuario) {
		// recuperación del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();
		String[] dataTag = tagUsuario.split(";");
		// Filtro para la consulta por precio y validados
		Filter filter = new FilterPredicate(Evento.VALIDADO, FilterOperator.EQUAL, true);
		
		// Query
		Query query = new Query(Evento.EVENTO_ENTITY).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventosRaw = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventosRaw;
		} else {
			for (Entity e : entities) {
				eventosRaw.add(new Evento(e));
			}
		}
		List<Evento> eventos= new ArrayList<Evento>();
		for (Evento e : eventosRaw) {
			for(String d: dataTag) {
				if(e.getTag().contains(d) && !eventos.contains(e)) {
					eventos.add(e);
				}
			}
		}
		return eventos;
	}

	public static List<Evento> getEventosNoValidados() {
		// recuperación del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// Filtro para la consulta
		Filter filter = new FilterPredicate(Evento.VALIDADO, FilterOperator.EQUAL, false);

		// Query
		Query query = new Query(Evento.EVENTO_ENTITY);

		// Ordenar query
		query.addSort(Evento.FECHAINICIO, Query.SortDirection.ASCENDING).setFilter(filter);

		// ejecuta la consulta, devolviendo una lista con un max. de FETCH_MAX_RESULTS
		// entidades
		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventos = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventos;
		} else {
			for (Entity e : entities) {
				eventos.add(new Evento(e));
			}
		}
		return eventos;
	}
	
	public static List<Evento> getEventosValidadosPorAlguien(Key usr) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Evento.VALIDADOR, FilterOperator.EQUAL, usr);

		Query query = new Query(Evento.EVENTO_ENTITY);

		query.addSort(Evento.FECHAINICIO, Query.SortDirection.ASCENDING).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventos = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventos;
		} else {
			for (Entity e : entities) {
				eventos.add(new Evento(e));
			}
		}
		return eventos;
	}
	public static List<Evento> getEventosCreadosPorAlguien(Key usr) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Evento.AUTOR, FilterOperator.EQUAL, usr);

		Query query = new Query(Evento.EVENTO_ENTITY);

		query.addSort(Evento.FECHAINICIO, Query.SortDirection.ASCENDING).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventos = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventos;
		} else {
			for (Entity e : entities) {
				eventos.add(new Evento(e));
			}
		}
		return eventos;
	}
	public static List<Evento> getEventosSeguidos(List<SeguirEvento> list) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Evento.AUTOR, FilterOperator.EQUAL, usr);

		Query query = new Query(Evento.EVENTO_ENTITY);

		query.addSort(Evento.FECHAINICIO, Query.SortDirection.ASCENDING).setFilter(filter);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Evento> eventos = new ArrayList<Evento>();
		if (entities.isEmpty()) {
			return eventos;
		} else {
			for (Entity e : entities) {
				eventos.add(new Evento(e));
			}
		}
		return eventos;
	}
}
