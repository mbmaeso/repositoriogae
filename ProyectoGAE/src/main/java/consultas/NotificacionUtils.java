package consultas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import entidadesBD.Comentario;
import entidadesBD.DSF;
import entidadesBD.Notificacion;

public class NotificacionUtils {

	private static final int FETCH_MAX_RESULTS = 50;

	public static void put(final String contenido, final Date fecha, final Key evento, 
			final Key usuario) {
        
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Notificacion notificacion = new Notificacion(contenido, fecha, evento, usuario);
		
		datastoreService.put(notificacion.getEntity());
	}
	
	public static List<Notificacion> getNotificaciones(Key usuario) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Notificacion.USUARIO, FilterOperator.EQUAL, usuario);

		Query query = new Query(Notificacion.FECHA).setFilter(filter);

		query.addSort(Notificacion.FECHA, Query.SortDirection.ASCENDING);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Notificacion> notificaciones = new ArrayList<Notificacion>();
		if (entities.isEmpty()) {
			return notificaciones;
		} else {
			for (Entity e : entities) {
				notificaciones.add(new Notificacion(e));
			}
		}
		return notificaciones;
	}
}
