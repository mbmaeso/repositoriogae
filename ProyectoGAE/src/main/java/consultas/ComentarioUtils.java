package consultas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import entidadesBD.Comentario;
import entidadesBD.DSF;
import entidadesBD.Evento;

public class ComentarioUtils {
	
	private static final int FETCH_MAX_RESULTS = 50;

	/**
	 * Insertar evento en en datastore
	 * 
	 * @param titulo,
	 *            tag, descripcion, precio, fechaInicio, fechaFin, validado,
	 *            validador, autor, latitud, longitud
	 */
	public static void delete(final Key key) {

		// recuperacion del datastore
		final DatastoreService datastoreService = DSF.getDatastoreService();

		// creamos un nuevo evento
		// los insertamos en el datastore
		datastoreService.delete(key);
	}

	public static void put(final Key e, final Key u, final String c ,final Date f) {
		final DatastoreService datastoreService = DSF.getDatastoreService();
		Comentario comentario = new entidadesBD.Comentario(e, u, c, f);
		datastoreService.put(comentario.getEntity());
	}

	public static void edit(Comentario comentario) {
		final DatastoreService datastoreService = DSF.getDatastoreService();
		datastoreService.put(comentario.getEntity());
	}

	public static List<Comentario> getComentarios(Key evento) {
		final DatastoreService datastoreService = DSF.getDatastoreService();

		Filter filter = new FilterPredicate(Comentario.EVENTO, FilterOperator.EQUAL, evento);

		Query query = new Query(Comentario.COMENTARIO_ENTITY).setFilter(filter);

		query.addSort(Comentario.FECHA, Query.SortDirection.ASCENDING);

		List<Entity> entities = datastoreService.prepare(query)
				.asList(FetchOptions.Builder.withLimit(FETCH_MAX_RESULTS));

		List<Comentario> comentarios = new ArrayList<Comentario>();
		if (entities.isEmpty()) {
			return comentarios;
		} else {
			for (Entity e : entities) {
				comentarios.add(new Comentario(e));
			}
		}
		return comentarios;
	}
}
