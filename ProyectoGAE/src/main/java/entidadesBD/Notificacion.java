package entidadesBD;

import java.util.Date;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import com.google.appengine.api.datastore.*;

public class Notificacion implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Key key;

	public static final String NOTIFICACION_ENTITY = "Notificacion";
	public static final String EVENTO = "evento";
	public static final String USUARIO = "usuario";
	public static final String CONTENIDO = "contenido";
	public static final String FECHA = "fecha";


	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
	private Entity entity;
	
	public Notificacion (final String contenido, final Date fecha, final Key evento, 
			final Key usuario) {
		entity = new Entity(NOTIFICACION_ENTITY);
		entity.setProperty(CONTENIDO, contenido);
		entity.setProperty(FECHA, fecha);
		entity.setProperty(EVENTO, evento);
		entity.setProperty(USUARIO, usuario);
	}

	public Notificacion(Entity entity) {
		this.entity = entity;
	}
	
	public Key getKey() {
		return entity.getKey();
	}

	public Entity getEntity() {
		return entity;
	}

	public String getContenido() {
		return (String) entity.getProperty(CONTENIDO);
	}
	
	public Date getFecha() {
		return (Date) entity.getProperty(FECHA);
	}

	public Key getEvento() {
		return (Key) entity.getProperty(EVENTO);
	}

	public Key getUsuario() {
		return (Key) entity.getProperty(USUARIO);
	}
}
