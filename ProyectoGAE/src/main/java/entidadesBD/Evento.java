package entidadesBD;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.appengine.api.datastore.*;

public class Evento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String EVENTO_ENTITY = "Evento";
	public static final String TITULO = "titulo";
	public static final String TAG = "tag";
	public static final String DESCRIPCION = "descripcion";
	public static final String PRECIO = "precio";
	public static final String FECHAINICIO = "fechaInicio";
	public static final String FECHAFIN = "fechaFin";
	public static final String VALIDADO = "validado";
	public static final String VALIDADOR = "validador";
	public static final String AUTOR = "autor";
	public static final String DIRECCION = "direccion";

	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
	private Entity entity;

	/* Constructor */

	public Evento(final String titulo, final String tag, final String descripcion, final double precio,
			final Date fechaInicio, final Date fechaFin, final boolean validado, final Key validador, final Key autor,
			final String direccion) {
		entity = new Entity(EVENTO_ENTITY);
		entity.setProperty(TITULO, titulo);
		entity.setProperty(DESCRIPCION, descripcion);
		entity.setProperty(TAG, tag);
		entity.setProperty(PRECIO, precio);
		entity.setProperty(FECHAINICIO, fechaInicio);
		entity.setProperty(FECHAFIN, fechaFin);
		entity.setProperty(VALIDADO, validado);
		entity.setProperty(VALIDADOR, validador);
		entity.setProperty(AUTOR, autor);
		entity.setProperty(DIRECCION, direccion);
	}

	public Evento(final Entity entity) {
		this.entity = entity;
	}

	/* Getters */
	public Key getKey() {
		return entity.getKey();
	}

	public Entity getEntity() {
		return entity;
	}

	public String getTitulo() {
		return (String) entity.getProperty(TITULO);
	}

	public void setTitulo(final String titulo) {
		entity.setProperty(TITULO, titulo);
	}

	public String getDescripcion() {
		return (String) entity.getProperty(DESCRIPCION);
	}

	public void setDescripcion(final String descripcion) {
		entity.setProperty(DESCRIPCION, descripcion);
	}

	public String getTag() {
		return (String) entity.getProperty(TAG);
	}

	public void setTag(final String tag) {
		entity.setProperty(TAG, tag);
	}

	public double getPrecio() {
		return (double) entity.getProperty(PRECIO);
	}

	public void setPrecio(final double precio) {
		entity.setProperty(PRECIO, precio);
	}

	/*
	 * public String getFechainicio() { return formatter.format((Date)
	 * entity.getProperty(FECHAINICIO)); }
	 */
	public Date getFechaInicio() {
		return (Date) entity.getProperty(FECHAINICIO);
	}

	public void setFechaInicio(final Date fechainicio) {
		entity.setProperty(FECHAINICIO, fechainicio);
	}

	/*
	 * public String getFechafin() { return formatter.format((Date)
	 * entity.getProperty(FECHAFIN)); }
	 */
	public Date getFechaFin() {
		return (Date) entity.getProperty(FECHAFIN);
	}

	public void setFechaFin(final Date fechafin) {
		entity.setProperty(FECHAFIN, fechafin);
	}

	public boolean getValidado() {
		return (boolean) entity.getProperty(VALIDADO);
	}

	public void setValidado(final boolean validado) {
		entity.setProperty(VALIDADO, validado);
	}

	public Key getValidador() {
		return (Key) entity.getProperty(VALIDADOR);
	}

	public void setValidador(final Key validador) {
		entity.setProperty(VALIDADOR, validador);
	}

	public Key getAutor() {
		return (Key) entity.getProperty(AUTOR);
	}

	public void setAutor(final Key autor) {
		entity.setProperty(AUTOR, autor);
	}

	public String getDireccion() {
		return (String) entity.getProperty(DIRECCION);
	}

	public void setDireccion(final String direccion) {
		entity.setProperty(DIRECCION, direccion);
	}

}
