package entidadesBD;

import java.io.Serializable;

import com.google.appengine.api.datastore.*;

public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String USUARIO_ENTITY = "Usuario";
	public static final String EMAIL = "email";
	public static final String NOMBRE = "nombre";
	public static final String APELLIDOS = "apellidos";
	public static final String TIPOUSUARIO = "tipousuario";
	public static final String TAG = "tag";
	public static final String DIRECCION = "direccion";

	private Entity entity;

	public Usuario(final String email, final String nombre, final String apellidos, final String tipousuario,
			final String tag, final String direccion) {
		entity = new Entity(USUARIO_ENTITY);
		entity.setProperty(EMAIL, email);
		entity.setProperty(NOMBRE, nombre);
		entity.setProperty(APELLIDOS, apellidos);
		entity.setProperty(TIPOUSUARIO, tipousuario);
		entity.setProperty(TAG, tag);
		entity.setProperty(DIRECCION, direccion);
	}

	public Usuario(final Entity entity) {
		this.entity = entity;
	}
	
	public Key getKey() {
		return entity.getKey();
	}

	public Entity getEntity() {
		return entity;
	}

	public String getEmail() {
		return (String) entity.getProperty(EMAIL);
	}

	public String getNombre() {
		return (String) entity.getProperty(NOMBRE);
	}

	public String getApellidos() {
		return (String) entity.getProperty(APELLIDOS);
	}

	public String getTipousuario() {
		return (String) entity.getProperty(TIPOUSUARIO);
	}

	public String getTag() {
		return (String) entity.getProperty(TAG);
	}

	public String getDireccion() {
		return (String) entity.getProperty(DIRECCION);
	}

	public void setEmail(String email) {
		entity.setProperty(EMAIL, email);
	}

	public void setNombre(String nombre) {
		entity.setProperty(NOMBRE, nombre);
	}

	public void setApellidos(String apellidos) {
		entity.setProperty(APELLIDOS, apellidos);
	}

	public void setTiposuario(String tipoUsuario) {
		entity.setProperty(TIPOUSUARIO, tipoUsuario);
	}

	public void setTag(String tag) {
		entity.setProperty(TAG, tag);
	}

	public void setDireccion(String direccion) {
		entity.setProperty(DIRECCION, direccion);
	}
}
