package entidadesBD;

import java.util.List;

import com.google.appengine.api.datastore.*;

public class SeguirEvento {

	public Key key;

	public static final String RELACION_ENTITY = "SeguirEvento";
	public static final String EVENTO = "evento";
	public static final String USUARIO = "usuario";

	private Entity entity = new Entity(RELACION_ENTITY);

	public SeguirEvento(final Key evento, final Key usuario) {
		entity.setProperty(EVENTO, evento);
		entity.setProperty(USUARIO, usuario);
	}

	public SeguirEvento(Entity entity) {
		this.entity = entity;
	}
	
	public Key getKey() {
		return entity.getKey();
	}

	public Entity getEntity() {
		return entity;
	}

	public Key getEvento() {
		return (Key) entity.getProperty(EVENTO);
	}

	public Key getUsuario() {
		return (Key) entity.getProperty(USUARIO);
	}
}
