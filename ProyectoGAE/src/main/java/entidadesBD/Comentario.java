package entidadesBD;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

public class Comentario implements Serializable{

	private static final long serialVersionUID = 1L;

	public static final String COMENTARIO_ENTITY = "Comentario";
	public static final String EVENTO = "Evento";
	public static final String USUARIO = "Usuario";
	public static final String COMENTARIO = "Comentario";
	public static final String FECHA = "Fecha";
	public static final String VOTOSFAVOR = "VotosFavor";
	public static final String VOTOSCONTRA = "VotosContra";

	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");
	private Entity entity;
	
	public Comentario (final Key e, final Key u, final String c ,final Date f) {
		entity = new Entity(COMENTARIO_ENTITY);
		entity.setProperty(EVENTO, e);
		entity.setProperty(USUARIO, u);
		entity.setProperty(COMENTARIO, c);
		entity.setProperty(FECHA, f);
		entity.setProperty(VOTOSFAVOR, 0);
		entity.setProperty(VOTOSCONTRA, 0);
	}

	public Comentario(final Entity entity) {
		this.entity = entity;
	}
	
	public Key getKey() {
		return entity.getKey();
	}

	public Entity getEntity() {
		return entity;
	}

	public Key getEvento() {
		return (Key) entity.getProperty(EVENTO);
	}
	
	public Key getUsuario() {
		return (Key) entity.getProperty(USUARIO);
	}
	
	public String getComentario() {
		return (String) entity.getProperty(COMENTARIO);
	}
	
	public Date getFecha() {
		return (Date) entity.getProperty(FECHA);
	}

	public int getVotosFavor() {
		return Integer.parseInt(entity.getProperty(VOTOSFAVOR)+"");
	}
	
	public int getVotosContra() {
		return Integer.parseInt(entity.getProperty(VOTOSCONTRA)+"");
	}

	public void setVotosFavor(int v) {
		entity.setUnindexedProperty(VOTOSFAVOR, v);
	}
	
	public void setVotosContra(int v) {
		entity.setUnindexedProperty(VOTOSCONTRA, v);
	}
}
